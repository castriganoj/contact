﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactV1.Workflows
{
    public class Menu
    {
        public void Execute()
        {
            string userInput = "0";

            do
            {
                DisplayMenu();
                userInput = Console.ReadLine();
                ProcessUserChoice(userInput);
            } while (userInput.ToUpper() != "Q");
        }

        private void ProcessUserChoice(string inputValue)
        {
            switch (inputValue)
            {
                case "1":
                    var workflow = new ListContacts();
                    workflow.Execute();
                    break;
                case "2":
                    var addWorkflow = new AddContact();
                    addWorkflow.Execute();
                    break;
            }
        }

        private void DisplayMenu()
        {
            Console.Clear();
            Console.WriteLine("***** Contacts Database *****\n");
            Console.WriteLine("1 - List Contacts");
            Console.WriteLine("2 - Add New Contact");
            Console.WriteLine("4 - Delete Contact");
            Console.WriteLine("\nQ to Quit");
            Console.WriteLine("\n\nEnter your choice: ");

        }
    }
}
