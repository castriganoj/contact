﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactV1.Data;
using ContactV1.Models;

namespace ContactV1.Workflows
{
    public class AddContact
    {
        public void Execute()
        {
            var repo = new ContactRepository();
            var contact = new Contact();

            Console.WriteLine("Enter First Name: ");
            contact.FirstName = Console.ReadLine();

            Console.WriteLine("Enter Last Name: ");
            contact.LastName = Console.ReadLine();

            Console.WriteLine("Enter Phone Number: ");
            contact.PhoneNumber = Console.ReadLine();

            Console.WriteLine("Enter Work Email: ");
            string wEmail = Console.ReadLine();
            Console.WriteLine("Enter Home Email: ");
            string hEmail = Console.ReadLine();
            contact.AddWorknHomeEmail(wEmail,hEmail);

            repo.AddContact(contact);

            var listWorkflow = new ListContacts();
            listWorkflow.Execute();
        }
    }
}
