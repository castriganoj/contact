﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactV1.Data;
using ContactV1.Utilities;

namespace ContactV1.Workflows
{
    public class ListContacts
    {
        public void Execute()
        {
            var repo = new ContactRepository();
            var contacts = repo.LoadAll();
            var printer = new ContactPrinter();
            printer.PrintList(contacts);
        }
    }
}
