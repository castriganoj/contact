﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactV1.Models
{
    public class Contact
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        private List<Email> EmailAddresses = new List<Email>();
 
    public void AddWorknHomeEmail(string Waddress, string Haddress)
    {
        Email eAddress  = new Email();
        eAddress.WorkEmail = Waddress;
        eAddress.HomeEmail = Haddress;
        EmailAddresses.Add(eAddress);

    }

        public List<Email> ReturnEmailList()
        {
            return EmailAddresses;
        }

    }
}
