﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactV1.Models
{
    public class Email
    {
        public string WorkEmail { get; set; }
        public string HomeEmail { get; set; }

        public Email()
        {
            WorkEmail = "N/A";
            HomeEmail = "N/A";
        }
    }

}
