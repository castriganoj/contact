﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactV1.Models;

namespace ContactV1.Data
{
    public class ContactRepository
    {
        private static List<Contact> _contacts = new List<Contact>();

        public ContactRepository()
        {
            if (_contacts.Count == 0)
            {
                _contacts.Add(new Contact() {FirstName = "Dave", LastName = "Balzer", PhoneNumber = "330-574-1551"});
                _contacts.Add(new Contact() {FirstName = "Jenny", LastName = "?", PhoneNumber = "867-5309"});
            }
        }

        public List<Contact> LoadAll()
        {
            return _contacts;
        }

        public void AddContact(Contact contact)
        {
            _contacts.Add(contact);
        }
    }
}
