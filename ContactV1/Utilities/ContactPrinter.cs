﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactV1.Models;

namespace ContactV1.Utilities
{
    public class ContactPrinter
    {
        public void PrintList(List<Contact> contacts)
        {
            Console.Clear();
            Console.WriteLine("Contact List");
            Console.WriteLine("-------------------------------------");
            if (contacts.Count == 0)
            {
                Console.WriteLine("You have no friends...   so sad...");
            }
            else
            {
                foreach (var c in contacts)
                {
                    Console.WriteLine("{0}, {1} {2,-10} {3, -15}", c.LastName, c.FirstName, c.PhoneNumber, c.ReturnEmailList());
                }
            }

            Console.WriteLine("\n\nPress any key to continue... ");
            Console.ReadKey();
        }
    }
}
