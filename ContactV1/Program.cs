﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactV1.Workflows;

namespace ContactV1
{
    class Program
    {
        static void Main(string[] args)
        {
            var menu = new Menu();
            menu.Execute();
        }
    }
}
